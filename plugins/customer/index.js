angular.module('luisdemoapp.customer', []).config(['c8yViewsProvider', 'c8yNavigatorProvider',
function (c8yViewsProvider, c8yNavigatorProvider) {
  'use strict';
  console.log(arguments);
  c8yViewsProvider.when('/device/:deviceId', {
    name: 'Customer',
    icon: 'user',
    priority: 0,
    templateUrl: ':::PLUGIN_PATH:::/views/index.html',
    controller: 'ld_MainCtrl'
  });

}])

.controller('ld_MainCtrl', ['$scope', '$routeParams', 'c8yInventory', 'c8yAlert',
function ($scope, $routeParams, c8yInventory, c8yAlert) {
  'use strict';

  var DEVICE_ID = $routeParams.deviceId;

  function onDevice(res) {
    $scope.customer = res.data.customerInfo || {};
  }

  function onCustomerSaved() {
    c8yAlert.success('Customer successfully saved');
  }

  function getDevice() {
    c8yInventory.detail(DEVICE_ID).then(onDevice);
  }

  function saveCustomer(c) {
    c8yInventory.save({
      id: DEVICE_ID,
      customerInfo: c
    }).then(onCustomerSaved);
  }

  $scope.saveCustomer = saveCustomer;

  getDevice();

}]);