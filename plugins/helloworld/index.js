angular.module('luisdemoapp.helloworld', []).config(['c8yNavigatorProvider', 'c8yViewsProvider',
function (c8yNavigatorProvider, c8yViewsProvider) {
  'use strict';

  c8yNavigatorProvider.addNavigation({
    name: 'Hello world',
    icon: 'cube',
    priority: 100000,
    path: ''
  });

  c8yViewsProvider.when('/', {
    templateUrl: ':::PLUGIN_PATH:::/views/index.html',
    controller: 'mh_MainCtrl'
  });

}]);